#include <iostream>
#include <armadillo>

using namespace arma;

void cg_FR(int niter, double tau0 = 0.1) {
    // Assuming A, objective, objective_gradient, Proj, U_op, W_op, matrix_generate
    // are all defined globally or within this scope as they were in the Python code.

    mat X0 = eye<mat>(A.n_rows, A.n_cols);
    mat grad0 = objective_gradient(X0);
    mat F0 = Proj(X0, grad0);
    mat Y0 = -F0;

    mat B0, Q0, R0;
    // Adjust the matrix_generate to output using reference parameters
    matrix_generate(X0, Y0, B0, Q0, R0);
    mat U0 = U_op(tau0, X0, B0, Q0, R0);
    mat W0 = W_op(tau0, X0, B0, Q0, R0);

    std::cout << std::setw(6) << "#iter" << std::setw(8) << "c" << std::setw(8) << "tau"
              << std::setw(15) << "gamma" << std::setw(15) << "norm grad" << std::setw(15) << "phi" << std::endl;

    for (int i = 0; i < niter; i++) {
        if (i == 0) {
            mat F = F0;
            mat Y = Y0;
            mat X = X0;
            mat U = U0;
            mat W = W0;
            double tau = tau0;
            continue;
        }

        double taumin = 0.001;
        double taumax = 1;
        double taue = 0.1 * std::max(taumin, tau);

        mat B, Q, R;
        matrix_generate(X0, Y0, B, Q, R);
        mat U = U_op(taue, X0, B, Q, R);
        mat W = W_op(taue, X0, B, Q, R);

        double p0 = objective(X0);
        double ptaue = objective(U * X0);
        double pp = trace(Y0.t() * objective_gradient(X0));

        double a = p0;
        double b = pp;
        double c = (ptaue - a - b*taue) / (taue * taue);

        if (c > 1e-6) {
            tau = -b / (2.0 * c);
        } else {
            tau = 2 * tau;
        }

        tau = std::min(tau, taumax);

        U = U_op(tau, X0, B, Q, R);
        W = W_op(tau, X0, B, Q, R);
        X = U * X0;
        F = Proj(X, objective_gradient(X));

        double gamma = trace((F - W * F0).t() * F) / trace(F0.t() * F0);
        Y = -F + gamma * (W * Y0);

       std::cout << std::setw(6) << i << std::setw(8) << c << std::setw(8) 
            << tau << std::setw(8) << gamma << std::setw(15) << 
            norm(objective_gradient(X), "fro") << std::setw(15) << objective(X)  << std::endl; 
       if (norm(objective_gradient(X), "fro") < 1e-6 || std::abs(objective(X) - objective(X0)) < 1e-10) 
        { 
            X.print(); 
            break; 
        } 
        X0 = X; 
        F0 = F; 
        Y0 = Y; 
        W0 = W; 
        tau0 = tau; 
        } 
        
    }