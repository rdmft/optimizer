#include <armadillo>
#include <iostream>
#include <cmath>

double stiefip(const arma::mat& Y, const arma::mat& A, const arma::mat& B) {
    arma::mat term = B - Y * (Y.t() * B) / 2;
    arma::cx_double result = arma::accu(arma::conj(A) % term);
    return result.real();
}

// void stiefgeod(const arma::mat& Y, const arma::mat& H,  arma::mat& Yt, arma::mat& Ht, double t=1.0) {
void stiefgeod(const arma::mat& Y, const arma::mat& H,  arma::mat& Yt, double t=1.0) {
    int n = Y.n_rows;
    int p = Y.n_cols;
    
    arma::mat A = Y.t() * H;
    A = (A - A.t()) / 2; // Ensure skew-symmetry
    
    arma::mat Q, R;
    arma::qr_econ(Q, R, H - Y * A);
    
    arma::mat MN = arma::expmat(t * arma::join_vert(arma::join_horiz(A, -R.t()), arma::join_horiz(R, arma::zeros(p, p))));
    // std::cout << "after  MN\n, size of MN " << MN.n_cols << " " << MN.n_rows << "\n";

    MN = MN.submat(0, 0, 2*p-1, p-1);
    
    // std::cout << "after line 18\n, size of MN " << MN.n_cols << " " << MN.n_rows << "\n";
    Yt = Y * MN.submat(0, 0, p-1, p-1) + Q * MN.submat(p, 0, 2*p-1, p-1);
    // std::cout << "after line 20\n";

    // Ht = H * MN.submat(0, 0, p-1, p-1) - Y * (R.t() * MN.submat(p, 0, 2*p-1, p-1));
}

arma::mat procrnt(const arma::mat& Y, const arma::mat& A, const arma::mat& B) {
    int n = Y.n_rows;
    int p = Y.n_cols;
    
    arma::mat AA = A.t() * A;
    arma::mat FY = AA * Y - A.t() * B;
    arma::mat YFY = Y.t() * FY;
    arma::mat G = FY - Y * YFY.t();

    int dimV = p * (p - 1) / 2 + p * (n - p);
    
    arma::mat H = arma::zeros(size(Y));
    arma::mat R0;
    arma::mat R1 = -G;
    arma::mat P = R1;
    arma::mat P0 = arma::zeros(size(Y));
    
    //This linear CG code is modified directly from Golub and Van Loan [45]

    double normR0;
    for (int k = 1; k <= dimV; ++k) {
        // double normR1 = arma::norm(R1, "fro");
        double normR1 = sqrt(stiefip(Y, R1, R1));
        if (normR1 < Y.n_elem * std::numeric_limits<double>::epsilon()) {
            break;
        }
        double beta = (k == 1) ? 0 : std::pow(normR1 / normR0, 2);
        
        P0 = P;
        P = R1 + beta * P;
        arma::mat FYP = FY.t() * P;
        arma::mat YP = Y.t() * P;
        arma::mat LP = AA * P - Y * (P.t() * AA * Y)
                      - Y * ((FYP - FYP.t()) / 2) 
                      - (P * YFY.t() - FY * YP.t()) / 2 
                      - (P - Y * YP) * (YFY / 2);
        
        // double alpha = std::pow(normR1, 2) / arma::accu(P % LP);
        double alpha = std::pow(normR1, 2) /stiefip(Y, P, LP);

        std::cout << "alpha=" <<alpha <<"\n";
        H += alpha * P;
        R0 = R1;
        normR0 = normR1;
        R1 -= alpha * LP;
    }
    
    return H;
}




int main() {
    int n = 5;
    int p = 3;

    arma::mat A = arma::randn(n, n);
    arma::mat B = A * arma::eye(n, p);
    arma::mat Y0 = arma::eye(n, p);

    arma::mat H = 0.1 * arma::randn(n, p);
    H = H - Y0 * (H.t() * Y0);

    // arma::mat Y = stiefgeod(Y0, H);
    arma::mat Y;
    arma::mat Ytemp;
    stiefgeod(Y0, H, Y);
    double d = arma::norm(Y - Y0, "fro");

    // while (d >arma::sqrt(arma::eps)) 
    while (d > 1e-10) 
    {
        stiefgeod(Y, procrnt(Y, A, B), Ytemp);
        Y = Ytemp;
        d = arma::norm(Y - Y0, "fro");
        std::cout << "d=" << d << std::endl;

        std::cout << "Y:\n" << Y << std::endl;
    }

    return 0;
}